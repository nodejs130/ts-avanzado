import { faker } from '@faker-js/faker';

import {
  addProduct,
  findProduct,
  products,
  updateProduct,
} from './products/product.service';

for (let index = 0; index < 50; index++) {
  addProduct({
    title: faker.commerce.productName(),
    stock: faker.number.int({ min: 10, max: 100 }),
    description: faker.commerce.productDescription(),
    image: faker.image.url(),
    color: faker.color.human(),
    size: faker.helpers.arrayElement(['S', 'M', 'L', 'XL']),
    price: parseInt(faker.commerce.price(), 10),
    categoryId: faker.string.uuid(),
    isNew: faker.datatype.boolean(),
    tags: faker.helpers.arrayElements(faker.word.words(5).split(' '), {
      min: 2,
      max: 5,
    }),
  });
}

console.log(products);

const product = products[0];
updateProduct(product.id, {
  title: 'Title update',
  stock: 8,
});

findProduct({
  stock: 10,
  color: 'red',
  tags: ['as', 'es'],
});
