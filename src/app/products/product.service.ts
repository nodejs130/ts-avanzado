import { faker } from '@faker-js/faker';

import {
  CreateProductDto,
  FindProductDto,
  UpdateProductDto,
} from './product.dto';
import { Product } from './product.model';

export const products: Product[] = [];

export const addProduct = (data: CreateProductDto): Product => {
  const newProduct: Product = {
    ...data,
    id: faker.string.uuid(),
    createdAt: faker.date.recent(),
    updatedAt: faker.date.recent(),
    category: {
      id: data.categoryId,
      name: faker.commerce.department(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    },
  };
  produtcs.push(newProduct);
  return newProduct;
};

export const updateProduct = (
  id: Product['id'],
  changes: UpdateProductDto
): Product => {
  const index = products.findIndex((p) => p.id === id);
  const prevData = products[index];
  products[index] = {
    ...prevData,
    ...changes,
  };
  return products[index];
};

export const findProduct = (dto: FindProductDto): Product[] => {
  // dto.color = 'blue';
  // dto.tags = [];
  // dto.tags?.pop();
  // dto.tags?.push('ve');
  return products;
};
