function parseString(input: string | string[]): string | string[] {
  if (Array.isArray(input)) {
    return input.join('');
  } else {
    return input.split('');
  }
}

const rArray = parseString('Lisa');
// rArray.reverse();
if (Array.isArray(rArray)) {
  rArray.reverse();
}
console.log('string to array', rArray);

const rString = parseString(['L', 'i', 's', 'a']);
// rString.toLowerCase();
if (typeof rString === 'string') {
  rString.toLowerCase();
}
console.log('array to string', rString);
