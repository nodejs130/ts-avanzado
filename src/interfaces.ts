type Sizes = 'S' | 'M' | 'L' | 'XL';
// type Product = {
//   id: string | number;
//   title: string;
//   createdAt: Date;
//   stock: number;
//   size?: Sizes;
// };
interface Product {
  id: string | number;
  title: string;
  createdAt: Date;
  stock: number;
  size?: Sizes;
}

const produtcs: Product[] = [];

produtcs.push({
  id: 1,
  title: 'P1',
  createdAt: new Date(),
  stock: 50,
});
