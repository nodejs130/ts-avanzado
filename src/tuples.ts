const prices: (number | string)[] = [1, 3, 2, 2, 2, 'test'];
prices.push(100);
prices.push('100');

let user: [string, number, boolean];
// user = ['lsimpson', 8];
// user = [8, 'lsimpson'];

// user = []
// user = ['lsimpson'];
user = ['lsimpson', 8, false];

const [username, age] = user;
console.log(username, age);
