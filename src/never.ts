const withoutEnd = () => {
  while (true) {
    console.log('nunca parar de aprender');
  }
};

const fail = (message: string) => {
  throw new Error(message);
};

const example = (input: unknown) => {
  if (typeof input === 'string') {
    return 'es un string';
  } else if (Array.isArray(input)) {
    return 'es un array';
  }
  return fail('not match');
};

console.log(example('Aca'));
console.log(example([3, 2, 5]));
console.log(example(1)); // Se detiene
console.log(example('Despues de fail'));
