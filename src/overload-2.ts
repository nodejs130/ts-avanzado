export function parseStr(input: string): string[];
export function parseStr(input: string[]): string;
export function parseStr(input: number): boolean;

// export function parseStr(input: string | string[]): string | string[] {
//   if (Array.isArray(input)) {
//     return input.join('');
//   } else {
//     return input.split('');
//   }
// }

export function parseStr(input: unknown): unknown {
  if (Array.isArray(input)) {
    return input.join('');
  } else if (typeof input === 'string') {
    return input.split('');
  } else if (typeof input === 'number') {
    return true;
  }
}

const rArray = parseStr('Lisa');
rArray.reverse();
// if (Array.isArray(rArray)) {
//   rArray.reverse();
// }
console.log('string to array', rArray);

const rString = parseStr(['L', 'i', 's', 'a']);
rString.toLowerCase();
// if (typeof rString === 'string') {
//   rString.toLowerCase();
// }
console.log('array to string', rString);

const rBoolean = parseStr(15);
console.log('number to boolean', rBoolean);
